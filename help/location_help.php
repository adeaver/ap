<?php

echo "Index Help";

?>
<?php include "dialogHeader.php"; ?>
<div role="main" class="ui-content ui-body-c ui-corner-bottom ui-overlay-shadow" data-role="content" data-theme="c">

			<h1>What's this?</h1>
<p>
This is the profile of a given location. You can see its name, address,
location on a map, and most importantly, whether it meets your or a comrade's
accessibility needs.

<p>
The percentages next to accessibility details relate to how frequently users
select or agree with that option for this location.

<p>
Click or press on "Add your review!" to enter your experience of the location's
accessibility. Simply check the items that the location has and click or press
"Submit".

<p>
Click or press on "Comments" at the bottom of the page to see what people are
saying about the location, and add feedback of your own.
			<p></p>
			<a class="ui-btn ui-btn-corner-all ui-shadow ui-btn-up-b" href="docs-dialogs.html" data-role="button" data-rel="back" data-theme="b"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Sounds good</span></span></a>       
		</div>
<?php include "dialogFooter.php"; ?>
