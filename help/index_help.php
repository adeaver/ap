<?php

echo "Index Help";

?>
<?php include "dialogHeader.php"; ?>
<div role="main" class="ui-content ui-body-c ui-corner-bottom ui-overlay-shadow" data-role="content" data-theme="c">

			<h1>About Accessible Places</h1>

<img src="/images/slogan.png" alt="built for, and powered by the people who need it" />

<p>
Accessible Places sheds light on our communities, easily showing how accessible
locations are. Our goal is to allow any user to plan their daily life by
mapping out how to navigate a traditionally poorly built physical space. By
contributing your experiences and observations you will help yourself and
others while increasing awareness of accessibility problems.

<p>
We are currently in a beta stage, which means that there are kinks being worked
out. One of the largest issues is that we are trying to gain access to better
data sets of regional information: locations of businesses, public parks, court
houses, etc. Until we clear this hurdle our search results will be somewhat
sparse.
			<p></p>
			<a class="ui-btn ui-btn-corner-all ui-shadow ui-btn-up-b" href="docs-dialogs.html" data-role="button" data-rel="back" data-theme="b"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Sounds good</span></span></a>       
		</div>
<?php include "dialogFooter.php"; ?>
