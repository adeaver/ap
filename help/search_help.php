<?php

echo "Index Help";

?>
<?php include "dialogHeader.php"; ?>
<div role="main" class="ui-content ui-body-c ui-corner-bottom ui-overlay-shadow" data-role="content" data-theme="c">

			<h1>What's all this?</h1>
<p>
Curated results are locations that people have added accessibility information
to, and are displayed first. Un-Curated results are still awaiting input from
users such as yourself.

<p>
Click or press on a name and address to view that result's information.
			<p></p>
			<a class="ui-btn ui-btn-corner-all ui-shadow ui-btn-up-b" href="docs-dialogs.html" data-role="button" data-rel="back" data-theme="b"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Sounds good</span></span></a>       
		</div>
<?php include "dialogFooter.php"; ?>
