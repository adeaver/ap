<?php
/*
 * Accessible Places - Location Display
 *
*/

$locationId = $_GET['id'];

$location = file_get_contents('http://www.accessibleplaces.in/api/v1/location/'.$locationId);
$location = json_decode($location);

$flags = file_get_contents("http://www.accessibleplaces.in/api/v1/location/flags");
$possibleFlags = json_decode($flags);

$reviewString = ( count($location->flags) > 0 ? 'Add your review!' : 'Be the first to rate / review this location!' );
?>

<?php require_once("header.php"); ?>
	<style>
		.profileLeft, .profileRight { width: 50%; float: left; text-align: left;}
		.profileLeft span {display: block; padding: 10px 5px; border-bottom: 1px solid #ccc;}
		.profileLeft span.last { border: none; }
	</style> 
	<div data-role="content" style="text-align: left;">
		<h2><?php echo $location->name; ?></h2>
		<div id="profileContainer" style="width: 100%;">
			<div class="profileLeft">
				<!-- <span id="name"><?php echo $location->name; ?></span> -->
				<span id="address" class="last"><?php echo $location->address; ?><div id="log"></div></span>
			</div>
			<div class="profileRight">
				<p id="geo-wrapper"><img /></p>
			</div>
		</div>
		<div style="clear: both;"></div>
		<style>
			.inactive { color: #ccc; }
		</style>
		<?php
		if ( count($location->flags) > 0 ) {
		?>
                  <!-- <h3>Available Features</h3> -->
                  <ul role="listbox" class="ui-listview ui-listview-inset ui-corner-all ui-shadow" data-role="listview" data-theme="d" data-inset="true">
	<li data-role="list-divider" style="font-size: 18px;">Available Features</li>

                    <?php
                    $location->flags = (array) $location->flags;
                    ksort($location->flags);

                    foreach($location->flags as $key => $value)
                    {
                      echo '<li class="ui-li ui-li-static ui-btn-up-d '.($i++ == 0 ? 'ui-corner-top ' : '').' active" tabindex="0" role="option">'.$possibleFlags->$key->label.'<span class="ui-li-count ui-btn-up-c">' . ( number_format($value/$location->numReports * 100) ) . '%</span></li>';
                      unset($possibleFlags->{$key});
                    }

                    unset($i);
                    ?>
                  </ul>
                <?php

                  if(count($possibleFlags))
                  {
                    $possibleFlags = (array) $possibleFlags;

//                    uasort($possibleFlags, function($a, $b) {
//                      if($a->label == $b->label)
//                        return 0;
//
//                    return ($a->label < $b->label) ? -1 : 1;
//                  });

                  ?>
                  <ul role="listbox" class="ui-listview ui-listview-inset ui-corner-all ui-shadow" data-role="listview" data-theme="d" data-inset="true">
	<li data-role="list-divider" style="font-size: 18px;"><?php echo ($location->flags) ? 'Un-Available Features' : 'Un-Curated Features';?></li>
                    <?php
                    foreach($possibleFlags as $value)
                      if($value->label)
                        echo '<li class="ui-li ui-li-static ui-btn-up-d '.($i++ == 0 ? 'ui-corner-top ' : '').' inactive" tabindex="0" role="option" data-theme="c">'. $value->label . '</li>';
                    ?>
                    <!-- <h3>Un-Available Features</h3> -->
<!--                    
<ul role="listbox" class="ui-listview ui-listview-inset ui-corner-all ui-shadow" data-role="listview" data-theme="d" data-inset="true">
	<li data-role="list-divider" style="font-size: 18px;">Un-Available Featuresi</li>


                      <?php
                      foreach($possibleFlags as $value)
                        if($value->label)
                          echo '<li class="ui-li ui-li-static ui-btn-up-d '.($i++ == 0 ? 'ui-corner-top ' : '').' inactive" tabindex="0" role="option" data-theme="c">'. $value->label . '</li>';
                      ?>
                    </ul>
-->
                  <?php
                  }
                }
                ?>
                <a id="helpDialog" href="/rate.php?id=<?php echo $locationId; ?>" class="ui-btn-left" data-icon="grid" data-role="button" data-rel="dialog" data-transition="slidedown"><?php echo $reviewString; ?></a>
<a href="/comments.php?id=<?php echo $locationId; ?>" data-role="button">Comments</a>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=<?php echo GOOGLE_API_KEY; ?>"></script>
<script>

if( navigator.userAgent.match(/Android/i) ||
 navigator.userAgent.match(/webOS/i) ||
 navigator.userAgent.match(/iPhone/i) ||
 navigator.userAgent.match(/iPod/i)
 ){
	$("#geo-wrapper").css({'width':'320px','height':'320px'});
	$('#profileContainer div').css('float','none');
	$(".profileRight").width("100%");
	$(".profileLeft").width("320px");
	$("#geo-wrapper").wrap('<div data-role="collapsible" data-collapsed="true"></div>');
	$("#geo-wrapper").before('<h3>Show Map</h3>');
	size = "320";

} else {
	size="430";
	$("#geo-wrapper").css({'width':'430px','height':'430px'});

}
	src = "http://maps.google.com/maps/api/staticmap?center=<?php echo $location->point->coordinates[1]; ?>,<?php echo $location->point->coordinates[0]; ?>&zoom=14&size=" + size + "x" + size + "&maptype=roadmap&markers=color:red|<?php echo $location->point->coordinates[1]; ?>,<?php echo $location->point->coordinates[0]; ?>&sensor=false";
	$("#geo-wrapper img").attr('src',src);

$(window).resize(function() {
$(".profileLeft").width($("#profileContainer").width() - 800);
});

$(document).ready(function() {
	$(".profileLeft").width($("#profileContainer").width() - 800);
	$("#helpDialog").attr('href','/help/location_help.php');
});
</script>

	</div><!-- /content -->
<?php require_once("footer.php"); ?>

