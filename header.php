<?php

require("config.php");
//First things first, get the help file link:
switch($_SERVER['PHP_SELF']) {
	case "/index.php":
		$helpLink = "/help/index_help.php";
		break;
	case "/search.php":
		$helpLink = "/help/search_help.php";
		break;
	case "/location.php":
		$helpLink = "/help/location_help.php";
		break;
}
?>
<!DOCTYPE html> 
<html> 
	<head> 
	<meta charset="utf-8" /> 
	<title>AccessiblePlaces.in Boston</title> 
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.0a3/jquery.mobile-1.0a3.min.css" /> 
	<script src="http://code.jquery.com/jquery-1.5.min.js"></script>
	<script src="/js/custom.js"></script>
	<script src="http://code.jquery.com/mobile/1.0a3/jquery.mobile-1.0a3.min.js"></script> 
    <script src="/star-rating/jquery.rating.js"></script>
    <script src="/star-rating/jquery.rating.pack.js"></script>
	<!--
	<link rel="stylesheet" href="/css/jquery.mobile-1.0a3.min.css" /> 
	<script src="/js/jquery-1.5.min.js"></script>
	<script src="/js/jquery.mobile-1.0a3.min.js"></script> 
	-->
	<link rel="stylesheet" href="/star-rating/jquery.rating.css" /> 
	<style>
.ui-header .ui-title {
    margin-right: 20px !important;
    margin-left: 20px !important;
}

.ui-footer .ui-title {
    margin-right: 0 !important;
    margin-left: 0 !important;
}

html, body {
    height: 100%;
}

body {
    background-color: #ddd;
}

#content {
   min-height: 100%;
}

.ui-page .ui-footer {
   position: absolute;
   bottom: 0;
}

	</style>

	</head>

<body style="text-align: center;"> 

 
<!-- Start of first page -->
<div data-role="page" id="content"> 

 
	<div data-role="header" data-theme="b" id="header"> 
		<h1>AccessiblePlaces</h1>
	<a id="helpDialog" href="<?php echo $helpLink;  ?> " class="ui-btn-left" data-icon="info" data-role="button" data-rel="dialog" data-transition="slidedown">Help</a>
		<?php if ( !isset($hideHome) || !$hideHome ) { ?>
		<a href="/" class="ui-btn-right" data-icon="home">Home</a>
		<?php } ?>
	</div><!-- /header --> 
	<div data-role="content">
 
 
 
