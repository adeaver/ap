<?php
/*
 * Accessible Places - Search Results
 *
*/
require_once("config.php");

$curatedArr = array("Samsons" => "0001", "Davids Pizza" => "00293", "Local Watering Hole" => "84923" );
$uncuratedArr = array("Samsons" => "0001", "Davids Pizza" => "00293", "Local Watering Hole" => "84923" );
$api = SERVER . "/api/v1/search/" . urlencode($_GET['q']);
if(!empty($_POST['lat'])) { $api . "/lat/" . $_POST['lat'] . "/long/" . $_POST['long']; }
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,  $api);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$cRes = curl_exec($ch);
curl_close($ch);

$results = json_decode($cRes);
?>

<?php include "header.php"; ?>
<?php
// Check for errors
if( $results && property_exists( $results, 'message' ) ) {
	echo '<script>';
	echo 'var errStr = "An error occured when trying to retreive your search results.  Click OK to go back to the homepage and try a different search.\n\n Message: ' . $results->message . '";';
	echo 'alert(errStr);';
	echo 'window.location = "/index.php"';
	echo '</script>';
}
?>
 
	<div data-role="content">	
		<h2>Search Results</h2>
		<?php //echo $api . "<br />"; ?>
		Total of <?php echo $results->numResults; ?> results.<br />
		<ul data-role="listview" data-theme="c" data-inset="true" type="curated">
		<li data-role="list-divider">Curated</li>
			<?php
				if($results->curated) {
					foreach ($results->curated as $key => $value) {
						echo '<li><a href="location.php?id='. $value->_id . '&type=curated">' . $value->name . "<br /><small>" . $value->address . "</small></a></li>";
					}
				} else {
					echo '<li> No curated results. </li>';
				}
			?>
		</ul>
		<ul data-role="listview" data-theme="c" data-inset="true" type="uncurated">
		<li data-role="list-divider">UnCurated</li>
			<?php
				if($results->notCurated) {
					foreach ($results->notCurated as $key => $value) {
						echo '<li><a href="location.php?id='. $value->_id . '&type=uncurated">' . $value->name . "<br /><small>" . $value->address . "</small></a></li>";
					}
				} else {
					echo '<li>No results.</li>';
				}
			?>
		</ul>
	<a href="/index.php" date-theme="b" data-role="button">New Search</a>		
		
	</div><!-- /content --> 
<script>
$(document).ready(function() {
	$("#helpDialog").attr('href','/help/search_help.php');
});
</script>
<?php require_once("footer.php"); ?>
