<?php
/*
 * Accessible Places - Submit Report
 *
*/

require_once("config.php");

$ch = curl_init();
$locationId = $_POST['_id'];

curl_setopt($ch, CURLOPT_URL,  SERVER . "/api/v1/location/".$_POST['_id']."/flags");
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$postData = 'flags[]='.implode('&flags[]=',$_POST['flags']);

curl_setopt($ch, CURLOPT_POSTFIELDS, $postData  );


$cRes = curl_exec($ch);
curl_close($ch);

?>

<?php include "header.php"; ?>
 
	<div data-role="content">	
		<h2>Thank you!</h2>
		<p>You are helping to make Boston a better and more accessible place.</p>
<div class="ui-grid-b">
	<div class="ui-block-a">&nbsp;</div>
	<div class="ui-block-b"><a href="location.php?id=<?php echo $locationId; ?>" data-role="button" data-icon="back" data-direction="reverse">Back</a></div>
	<div class="ui-block-c"></div>
</div>
	</div><!-- /content -->
	<script> 
	 $(document).ready(function() {
		$("#header").attr('data-backbtn','false')
	});
	</script>
<?php require_once("footer.php"); ?>
