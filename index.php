<?php
/*
 * Accessible Places - Index
 *
*/

$hideHome = true;

require_once("header.php");
?>

	<script>
	var defaultQuery;

	function handleFocus() {
		$q = $('#query');
		if ( $q.hasClass('default') ) {
			defaultQuery = $q.val();
			$q.val('').removeClass('default');
		}
	}

	function handleBlur() {
		$q = $('#query');
		if ( $q.val() == '' ) $q.val(defaultQuery).addClass('default');
	}
	</script>

	<style>
		.default { color: #aaa; }
		#searchForm { max-width: 500px; margin: 0 auto; }
		#searchForm .ui-input-search { display: block; width: auto; }
		#searchForm .ui-input-search input { display: block; width: 98%; }
	</style>
 
		<img id="logo" src="/images/logo550.png" />

                <p style="max-width:500px;margin-left:auto; margin-right:auto;">
                  Search for places and see how accessible they really are. For example, try
                  searching for <a href="/search.php?q=boston+public+library" style="white-space: nowrap;" id="exampleSearch">Boston Public Library</a>.

		<h2 style="margin-bottom: 5px;">Search</h2> 
		<form action="search.php" method="get" id="searchForm">
		<div data-role="fieldcontain" style="padding-top: 0;margin-top: 0;">
		    <input type="search" name="q" id="query" value="Name of Location" class="default" onblur="handleBlur()" onfocus="handleFocus()" />
		    <button type="submit" data-theme="b">Submit</button>
		</div>

		</form>
		<div id="live-geolocation">Retreiving your location...</div>

<script src="http://code.google.com/apis/gears/gears_init.js"></script>
<script src="http://geo-location-javascript.googlecode.com/svn/trunk/js/geo.js"></script>
	
<script>

function supports(bool, suffix) {
      var s = "";
      if (bool) {
        s += "Your location: ";
      } else {
        s += " Your browser does not support " + suffix + ". :(";
      }
      return s;
    }

function lookup_location() {
      geo_position_js.getCurrentPosition(show_map, show_map_error);
    }
    function show_map(loc) {
      var geo = loc.coords.latitude + ", " + loc.coords.longitude;
        $("#live-geolocation").html(supports(true) + geo);
	$("#query").before("<input type='hidden' name='lat' id='lat' value='" + loc.coords.latitude + "' />");
	$("#query").before("<input type='hidden' name='long' id='long' value='" + loc.coords.longitude + "' />");

        var exSearch = $("#exampleSearch");
        $(exSearch).attr('href', $(exSearch).attr('href')+'&lat='+loc.coords.latitude+'&lon='+loc.coords.longitude);
    }
    function show_map_error() {
      $("#live-geolocation").html('Unable to determine your location.');
    }
    $(document).ready(function() {
      if (geo_position_js.init()) {
       lookup_location();
      } else {
        $("#live-geolocation").html(supports(false, "geolocation"));
     }
    });

if( navigator.userAgent.match(/Android/i) ||
 navigator.userAgent.match(/webOS/i) ||
 navigator.userAgent.match(/iPhone/i) ||
 navigator.userAgent.match(/iPod/i)
 ){
	var src = "/images/logo280.png";
	$("#logo").attr('src',src);
	$("#logo").width(280);
}
 
</script>
	
<?php require_once("footer.php"); ?>
